using System;


namespace QJ.Video
{

    public enum EVideoInput
    {
        PLAY,
        PAUSE,
        STOP,
        TOGGLE_LOOP,
        GO_BACK_BY_SECONDS,
        GO_FORWARD_BY_SECONDS,
    }


    public enum EAudioInput
    {
        CHANGE_VOLUME,
        TOGGLE_MUTE,
    }


    public delegate void AudioInputDelegate(EAudioInput input, Object arg = null);


    public delegate void VideoInputDelegate(EVideoInput input, Object arg = null);


}