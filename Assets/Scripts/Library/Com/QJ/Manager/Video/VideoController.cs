﻿using QJ.Video.Internal;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace QJ.Video
{

    /// <summary>
    /// The multiton video controller.
    /// </summary>
    public class VideoController
    {
        /// <summary>
        /// object to hold the ump player component
        /// </summary>
        private GameObject m_controllerObject;

        private IVideoView m_videoView;
        private IAudioView m_audioView;
        private VideoPlayer m_player;


        #region FACTORY

        public const string MAIN_INSTANCE = "main_instance";

        private static Transform m_rootTransform;

        /// <summary>
        /// Dictionary containing the instances
        /// </summary>
        private static Dictionary<string, VideoController> m_instances = new Dictionary<string, VideoController>();


        /// <summary>
        /// Create new or get existing instance using instance id
        /// </summary>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public static VideoController GetInstance(string instanceId = MAIN_INSTANCE)
        {
            if(m_rootTransform == null)
                m_rootTransform = new GameObject("QJ.Video").transform;

            if(!m_instances.ContainsKey(instanceId))
                m_instances.Add(instanceId, new VideoController(m_rootTransform, instanceId));

            return m_instances[instanceId];
        }


        /// <summary>
        /// Destroys existing instance using instance id
        /// </summary>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public static void DestroyInstance(string instanceId)
        {
            if(!m_instances.ContainsKey(instanceId)) return;

            GetInstance(instanceId).Dispose();
            m_instances.Remove(instanceId);
        }

        #endregion


        private VideoController(Transform rootTransform, string _instanceId)
        {
            // purpose of gameobject is to attach video player component
            this.m_controllerObject = new GameObject($"{_instanceId}");
            this.m_controllerObject.transform.SetParent(rootTransform);
        }


        public void InitVideoView(IVideoView videoView)
        {
            if (this.m_player != null)
                throw new InvalidOperationException("cannot initialise player a second time.");

            this.m_player = new VideoPlayer(this.m_controllerObject, videoView);

            if (this.m_videoView != null)
                throw new InvalidOperationException("cannot initialise video view a second time.");

            this.m_videoView = videoView;
            this.m_videoView.onInput += this.HandleVideoInput;
        }


        public void InitAudioView(IAudioView audioView)
        {
            if (this.m_audioView != null)
                throw new InvalidOperationException("cannot initialise audio view a second time.");

            this.m_audioView = audioView;
            this.m_audioView.onInput += this.HandleAudioInput;
        }


        public void SetVideoUrlOrFilePath(string url)
        {
            this.m_player?.SetVideoUrl(url);
        }


        public void TriggerPlay()
        {
            this.m_player?.StartVideoPlayback();
        }


        public void TriggerPause()
        {
            this.m_player?.PauseVideoPlayback();
        }


        public void TriggerStop()
        {
            this.m_player?.StopVideoPlayback();
        }

        public void TriggerLoop(bool isLooping)
        {
            this.m_player?.SetVideoLooping(isLooping);
        }

        public void TriggerRewind(float seconds)
        {
            this.m_player?.RewindVideo(seconds);
        }

        public void TriggerFastForward(float seconds)
        {
            this.m_player?.FastForwardVideo(seconds);
        }

        public void TriggerMuteToggle(bool isMuted)
        {
            this.m_player?.ToggleMuteAudio(isMuted);
        }

        public void TriggerVolumeChange(float normalisedVolume)
        {
            this.m_player?.ChangeAudioVolume(normalisedVolume);
        }


        private void HandleVideoInput(EVideoInput videoInput, object arg)
        {
            switch(videoInput)
            {
                case EVideoInput.PLAY:
                    this.TriggerPlay();
                    break;

                case EVideoInput.PAUSE:
                    this.TriggerPause();
                    break;

                case EVideoInput.STOP:
                    this.TriggerStop();
                    break;

                case EVideoInput.TOGGLE_LOOP:
                    this.TriggerLoop((bool)arg);
                    break;

                case EVideoInput.GO_BACK_BY_SECONDS:
                    this.TriggerRewind((float)arg);
                    break;

                case EVideoInput.GO_FORWARD_BY_SECONDS:
                    this.TriggerFastForward((float)arg);
                    break;
            }
        }


        private void HandleAudioInput(EAudioInput input, object arg)
        {
            switch(input)
            {
                case EAudioInput.CHANGE_VOLUME:
                    TriggerVolumeChange((float)arg);
                    break;

                case EAudioInput.TOGGLE_MUTE:
                    TriggerMuteToggle((bool)arg);
                    break;
            }
        }
        

        private void Dispose()
        {
            if(this.m_videoView != null)
            {
                this.m_videoView.onInput -= this.HandleVideoInput;
            }

            if(this.m_audioView != null)
            {
                this.m_audioView.onInput -= this.HandleAudioInput;
            }

            this.m_player?.Dispose();
        }
    }

}