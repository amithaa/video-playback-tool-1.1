﻿using UnityEngine;


namespace QJ.Video
{
    public interface IVideoView
    {
        event VideoInputDelegate onInput;

        /// <summary>
        /// Returns all rendering target surfaces from the same controller instance
        /// (they will play the same video).
        /// </summary>
        /// <returns></returns>
        GameObject[] GetRenderTargets();
    }
}
