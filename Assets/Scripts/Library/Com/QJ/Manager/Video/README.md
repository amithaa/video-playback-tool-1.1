# QJ Video

### Version 1.0

Last updated on 27-Jan-2021

- [change] Input for combining multiple actions into one delegate
- [change] VideoPlayer as a wrapper for Universal Media Player class
- [new] SharedAudioViewDemo.cs to showcase IAudioView implementation

### Version 0.1

Last updated on 22-Jan-2021

- [new] multi-ton VideoController to handle multiple video views
- [new] MainDemo.cs to showcase VideoController initialisation
- [new] VideoViewDemo.cs to showcase IVideoView implementation 
- [new] uml diagram