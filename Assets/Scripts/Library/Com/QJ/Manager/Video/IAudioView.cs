﻿namespace QJ.Video
{
    public interface IAudioView
    {
        event AudioInputDelegate onInput;
    }
}
