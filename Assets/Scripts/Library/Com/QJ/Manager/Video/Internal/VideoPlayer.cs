﻿using System;
using UMP;
using UnityEngine;
using Object = UnityEngine.Object;


namespace QJ.Video.Internal
{
    /// <summary>
    /// A wrapper class to UniversalMediaPlayer for video operations.
    /// </summary>
    public class VideoPlayer
    {
        private UniversalMediaPlayer m_umpPlayer;
        private float m_maxVolume = 100f;


        public VideoPlayer(GameObject controllerObject, IVideoView videoView)
        {
            this.m_umpPlayer = controllerObject.GetComponent<UniversalMediaPlayer>();

            if(this.m_umpPlayer == null)
                this.m_umpPlayer = controllerObject.AddComponent<UniversalMediaPlayer>();

            this.m_umpPlayer.RenderingObjects = videoView.GetRenderTargets();
        }

        public void StartVideoPlayback()
        {
            m_umpPlayer.Play();
        }

        /// <summary>
        /// Sets the relative path or url for video 
        /// Example of using:
        /// Local storage space - 'Scripts/Library/Com/QJ/Manager/Video/Demo/video.mp4'
        /// Remote space (streams) - 'rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov';
        /// </summary>
        public void SetVideoUrl(string videoUrl)
        {
            this.m_umpPlayer.Path = videoUrl;
        }

        public void PauseVideoPlayback()
        {
            m_umpPlayer.Pause();
        }

        public void StopVideoPlayback()
        {
            m_umpPlayer.Stop();
        }

        public void ReplayVideo()
        {
            throw new NotImplementedException();
        }

        public void RewindVideo(float step)
        {
            var _currentPosition = m_umpPlayer.Position;

            if (_currentPosition - step >= 0)
                m_umpPlayer.Position = _currentPosition - step;
            else
                m_umpPlayer.Position = 0;
        }

        public void FastForwardVideo(float step)
        {
            var _currentPosition = m_umpPlayer.Position;

            if (_currentPosition + step <= m_umpPlayer.Length)
                m_umpPlayer.Position = _currentPosition + step;
            else
                m_umpPlayer.Position = 1;
        }

        public void SetVideoLooping(bool isLooping)
        {
            m_umpPlayer.Loop = isLooping;
        }

        public void ResetVideoResolution()
        {
            throw new NotImplementedException();
        }

        public void AdjustVideoResolution(int videoWidth, int videoHeight)
        {
            throw new NotImplementedException();
        }

        public void ToggleFullScreenVideo(bool isFullScreen)
        {
            throw new NotImplementedException();
        }

        public void SkipToTime(int timeStamp)
        {
            throw new NotImplementedException();
        }

        public void ToggleMuteAudio(bool isMuted)
        {
            m_umpPlayer.Mute = isMuted;
        }

        public void SetAudioVolume(float volume)
        {
            m_umpPlayer.Volume = volume;
        }

        public void ChangeAudioVolume(float normalisedVolume)
        {
            m_umpPlayer.Volume = normalisedVolume * m_maxVolume;
        }

        public void LimitAudioVolume(int limit)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            if(!this.m_umpPlayer) return;

            Object.Destroy(this.m_umpPlayer.gameObject);
            this.m_umpPlayer = null;
        }
    }
}
