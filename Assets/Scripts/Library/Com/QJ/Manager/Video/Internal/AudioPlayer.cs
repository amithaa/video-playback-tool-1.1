﻿// using System;
// using UMP;
//
// namespace QJ.Video.Internal
// {
//     public class AudioPlayer
//     {
//         private UniversalMediaPlayer m_umpPlayer;
//         private IAudioView m_view;
//
//         private AudioOutput[] m_audioOutputs;
//         private MediaTrackInfo[] m_audioTracks;
//         private MediaTrackInfo[] m_spuTracks;
//
//         private float m_volume;
//         public bool isMuted;
//
//         public AudioPlayer(UniversalMediaPlayer umpPlayer, IAudioView view)
//         {
//             m_umpPlayer = umpPlayer;
//             m_view = view;
//
//             m_view.AddOnClickMuteListener(MuteAudio);
//             m_view.AddOnVolumeChangeListener(SetAudioVolume);
//         }
//
//         /// <summary>
//         /// Get simple array that consist with Unity 'AudioSource' components
//         /// (will be used for audio data output instead default one)
//         /// * Warning: in current stage correctly working only with audio that has 2 channels
//         /// </summary>
//         public AudioOutput[] AudioOutputs
//         {
//             get
//             {
//                 return m_audioOutputs;
//             }
//         }
//
//         /// <summary>
//         /// Get the available audio tracks
//         /// </summary>
//         public MediaTrackInfo[] AudioTracks
//         {
//             get
//             {
//                 return m_audioTracks;
//             }
//         }
//
//         /// <summary>
//         /// Gets the available spu (subtitle) tracks (supported only on Standalone platform)
//         /// </summary>
//         public MediaTrackInfo[] SpuTracks
//         {
//             get
//             {
//                 return m_spuTracks;
//             }
//         }
//
//         public void SetAudioOptions(AudioOutput[] audioOutputs)
//         {
//             throw new NotImplementedException();
//         }
//
//         public void SetAudioVolume(float volume)
//         {
//             throw new NotImplementedException();
//         }
//
//         public void MuteAudio()
//         {
//             throw new NotImplementedException();
//         }
//
//         public void SetSpuTracks(MediaTrackInfo[] spuTracks)
//         {
//             throw new NotImplementedException();
//         }
//
//         public void SetAudioTracks(MediaTrackInfo[] spuTracks)
//         {
//             throw new NotImplementedException();
//         }
//
//         public void Dispose()
//         {
//             throw new NotImplementedException();
//         }
//     }
// }
