﻿using UnityEngine;

namespace QJ.Video.Demo
{
    public class SharedAudioViewDemo : MonoBehaviour, IAudioView
    {
        public event AudioInputDelegate onInput;

        [SerializeField] private KeyCode muteToggleKey;
        [SerializeField] private KeyCode volumeUpKey;
        [SerializeField] private KeyCode volumeDownKey;

        private bool m_isMuted = false;

        void Update()
        {
            if (Input.GetKeyDown(volumeUpKey))
            {
                onInput?.Invoke(EAudioInput.CHANGE_VOLUME, .8f);
            }
            else if (Input.GetKeyDown(volumeDownKey))
            {
                onInput?.Invoke(EAudioInput.CHANGE_VOLUME, 0.3f);
            }
            else if (Input.GetKeyDown(muteToggleKey))
            {
                if (m_isMuted)
                    onInput?.Invoke(EAudioInput.TOGGLE_MUTE, false);
                else
                    onInput?.Invoke(EAudioInput.TOGGLE_MUTE, true);

                m_isMuted = !m_isMuted;
            }
        }
    }
}
