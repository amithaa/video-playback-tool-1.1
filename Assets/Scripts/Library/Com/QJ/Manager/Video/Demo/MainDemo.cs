﻿using System.IO;
using UnityEngine;

namespace QJ.Video.Demo
{
    public class MainDemo : MonoBehaviour
    {
        [SerializeField] private string[] urls;
        [SerializeField] private string[] filePaths;
        [SerializeField] private VideoViewDemo[] m_videoViews;
        [SerializeField] private SharedAudioViewDemo m_audioView;

        void Start()
        {
            //Videos with the urls
            for (int i = 0; i < this.urls.Length && m_videoViews.Length > i; i++)
            {
                // create 1 new controller instance for each video view
                if (m_videoViews[i] != null)
                {
                    var _controller = VideoController.GetInstance($"video manager {i}");
                    _controller.InitVideoView(m_videoViews[i]);

                    //Init the audio view
                    _controller.InitAudioView(m_audioView);

                    _controller.SetVideoUrlOrFilePath(this.urls[i]);
                }
            }

            //Videos with the filepath
            for (int j = this.urls.Length; j < this.urls.Length + this.filePaths.Length && m_videoViews.Length > j; ++j)
            {
                if (m_videoViews[j] != null)
                {
                    var _controller = VideoController.GetInstance($"video manager {j}");
                    _controller.InitVideoView(m_videoViews[j]);

                    //Init the audio view
                    _controller.InitAudioView(m_audioView);

                    var _fileLocation = Path.Combine(Application.dataPath, this.filePaths[j - this.urls.Length]);
                    _controller.SetVideoUrlOrFilePath(_fileLocation);
                }
            }
        }

    }
}
