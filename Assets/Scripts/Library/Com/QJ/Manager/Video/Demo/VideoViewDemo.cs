﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace QJ.Video.Demo
{

    public class VideoViewDemo : MonoBehaviour, IVideoView
    {
        [SerializeField] private MeshRenderer[] targetRenderers;
        [SerializeField] private RawImage[] targetImages;
        [SerializeField] private KeyCode playPauseToggleKey;
        [SerializeField] private KeyCode rewindKey;
        [SerializeField] private KeyCode fastForwardKey;
        [SerializeField] private KeyCode loopToggleKey;

        public event VideoInputDelegate onInput;

        private bool m_isPlaying = false;
        private bool m_Looping = false;


        void Update()
        {
            // Shift + toggle key to stop video
            if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                if(Input.GetKeyDown(playPauseToggleKey))
                {
                    onInput?.Invoke(EVideoInput.STOP);
                    this.m_isPlaying = false;
                }
            }
            // toggle key to play/pause video
            else if(Input.GetKeyDown(playPauseToggleKey))
            {
                if(m_isPlaying)
                    onInput?.Invoke(EVideoInput.PAUSE);
                else
                    onInput?.Invoke(EVideoInput.PLAY);

                this.m_isPlaying = !this.m_isPlaying;
            }

            //key to rewind
            else if (Input.GetKeyDown(rewindKey))
            {
                this.onInput?.Invoke(EVideoInput.GO_BACK_BY_SECONDS, 0.05f);
            }

            //key to fast-forward
            else if (Input.GetKeyDown(fastForwardKey))
            {
                this.onInput?.Invoke(EVideoInput.GO_FORWARD_BY_SECONDS, 0.05f);
            }

            //key to loop
            else if (Input.GetKeyDown(loopToggleKey))
            {
                if (m_Looping)
                    this.onInput?.Invoke(EVideoInput.TOGGLE_LOOP, false);
                else
                    this.onInput?.Invoke(EVideoInput.TOGGLE_LOOP, true);

                this.m_Looping = !this.m_Looping;
            }
        }


        public GameObject[] GetRenderTargets()
        {
            List<GameObject> _targets = new List<GameObject>();

            foreach(var _r in this.targetRenderers)
                _targets.Add(_r.gameObject);

            foreach(var _i in this.targetImages)
                _targets.Add(_i.gameObject);

            return _targets.ToArray();
        }
    }

}